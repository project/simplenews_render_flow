This module adds a button in content type's specific layout builder to dump default rendered layout to
the same node html field.

The target field will be based on what's defined in node type settings(section workflow).

There you could choose any `text_long` to dump the specified view mode result. By now only `Full` view
mode can be choosen, because when using layout builder only the **Full** view mode allows each content
item to have its layout customized, this restriction may change when https://www.drupal.org/project/drupal/issues/2907413
is fixed.

## Solving the FULL view mode waste

Because of the above restriction, **Full** view mode will always show the 'live' build of the content, e.g. that
means that if layout builder specifies the use of dynamic views it will contain the recent content. This could
be solved using a block that renders only the defined render field, and allowing the visualization of the
block only if content type is simplenews. This block is included in this module it's up to you to place it
where is needed and also find other ways to hide the original **Full** view mode content, for example using css.

## Configure the simplenews render flow

- Create a `text_long` field in content used as simplenews.
- Define the render field which will be the target of the html dump in `/admin/structure/types/manage/your_simplenews_bundle` . Only **Full** view mode can be selected by now.
- Edit the view modes **Email: HTML** and maybe **Email: Plain** to show only the defined render field.
- If you plan to use _Layout Builder_(recommended) in `/admin/structure/types/manage/your_simplenews_bundle/display/default` check *Use Layout Builder* (and maybe *Allow each content item to have its layout customized*).
- Edit the simplenews content type default layout for **Full** view mode (or **default** if **Full** is not overriden) in `/admin/structure/types/manage/your_simplenews_bundle/display/default` pressing *manage layout* button.
Note that when defining the Layout builder contents **DO NOT** add the render field to avoid render build loops that
will exhaust your machine memory.

## Defining simplenews layout or contents

After setting, for all bundle, or for every content item if choosen, one can build its layout.
With the Layout builder you could control how a newsletter is displayed and which content must get in. When you
are sure it\'s fine press the **Render to html field** button, it will output this layout to defined target field
and the content of this field will be used as newsletter item content.
The result will be used as **send** and **archive** contents. The render output without site other sections such
as header, sidebars, footers... that can be used as **archive** will be shown at **mail preview** tab.
If the workaround explained in [solving the FULL view mode waste section](#solving-the-full-view-mode-waste) is
used, canonical url of the content such as `https://example.org/node/{nid}` could be used to show the render
field, the live layout or both.

TODO:
- choose the layout to be used to dump (if feature in  https://www.drupal.org/project/drupal/issues/2907413 is fixed).
