<?php

namespace Drupal\simplenews_render_flow\Controller;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Access\AccessResultInterface;
use Drupal\Core\Cache\Cache;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Mail\MailManagerInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\node\NodeInterface;
use Drupal\simplenews\Entity\Subscriber;
use Drupal\simplenews\Mail\MailCacheNone;
use Drupal\simplenews\Mail\MailEntity;
use Drupal\symfony_mailer\EmailFactoryInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Response;

/**
 * Returns responses for Simplenews Render Flow routes.
 */
class SimplenewsRenderFlowController extends ControllerBase {

  use StringTranslationTrait;

  /**
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The language manager.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  protected $languageManager;

  /**
   * The mail manager.
   *
   * @var \Drupal\Core\Mail\MailManagerInterface
   */
  protected $mailManager;

  /**
   * A date time instance.
   *
   * @var \Drupal\Component\Datetime\TimeInterface
   */
  protected $time;

  /**
   * The email factory service.
   *
   * @var \Drupal\symfony_mailer\EmailFactoryInterface
   */
  protected $emailFactory;

  /**
   * Inject entityTypeManager.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager service.
   * @param \Drupal\Core\Language\LanguageManagerInterface $language_manager
   *   The language manager.
   * @param \Drupal\Core\Mail\MailManagerInterface $mail_manager
   *   The mail manager.
   * @param \Drupal\Component\Datetime\TimeInterface $time
   *   A date time instance.
   * @param \Drupal\symfony_mailer\EmailFactoryInterface $email_factory
   *   The email factory service.
   */
  public function __construct(EntityTypeManagerInterface $entityTypeManager, LanguageManagerInterface $language_manager, MailManagerInterface $mail_manager, TimeInterface $time, EmailFactoryInterface $email_factory) {
    $this->entityTypeManager = $entityTypeManager;
    $this->languageManager = $language_manager;
    $this->mailManager = $mail_manager;
    $this->time = $time;
    $this->emailFactory = $email_factory;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('language_manager'),
      $container->get('plugin.manager.mail'),
      $container->get('datetime.time'),
      $container->get('email_factory')
    );
  }

  /**
   * Builds the response.
   */
  public function buildMailSystem($node = NULL) {
    $cache = new MailCacheNone();
    $subscriber = Subscriber::loadByMail("hya27psuh@mozmail.com", 'create', $this->languageManager->getCurrentLanguage());
    $mail = new MailEntity($node, $subscriber, $cache);
    $params['simplenews_mail'] = $mail;

    // Rebuild cache by assetResolver (Cache::PERMANENT) in Drupal\Core\Asset\AssetResolver::getCssAssets.
    \Drupal::service('asset.css.collection_optimizer')->deleteAll();
    $this->state()->set('system.css_js_query_string', base_convert($this->time->getCurrentTime(), 10, 36));
    // The only valid clearer:
    Cache::invalidateTags(['library_info']);

    $composed_mail = $this->mailManager->mail('simplenews', $mail->getKey(), $mail->getRecipient(), $mail->getLanguage(), $params, $mail->getFromFormatted(), FALSE);

    $response = new Response();
    $response->setContent($composed_mail['body']);
    // $response->headers->set('Content-Type', 'text/xml');
    return $response;

    // If a render arrray is want:
    $build['content'] = [
      '#type' => 'item',
      '#markup' => $this->t('It works!'),
    ];
    $output = \Drupal::service('renderer')->renderRoot($build);

    return $build;
  }

  /**
   * Builds the response. obtaining pure html and rendering just this element alone in DOM.
   */
  public function buildAlone($node = NULL) {
    $cache = new MailCacheNone();
    $subscriber = Subscriber::loadByMail("hya27psuh@mozmail.com", 'create', $this->languageManager->getCurrentLanguage());
    $mail = new MailEntity($node, $subscriber, $cache);
    $params['simplenews_mail'] = $mail;
    $params['simplenews_subscriber'] = $subscriber;

    // Rebuild cache by assetResolver (Cache::PERMANENT) in Drupal\Core\Asset\AssetResolver::getCssAssets.
    \Drupal::service('asset.css.collection_optimizer')->deleteAll();
    $this->state()->set('system.css_js_query_string', base_convert($this->time->getCurrentTime(), 10, 36));
    // The only valid clearer:
    Cache::invalidateTags(['library_info']);

    // Forge the message array IF not send it will not pass the process phase of the message.
    $composed_mail = $this->mailManager->mail('simplenews', $mail->getKey(), $mail->getRecipient(), $mail->getLanguage(), $params, $mail->getFromFormatted(), TRUE);

    $output = \Drupal::service('renderer')->renderRoot($composed_mail['body']);
    $response = new Response();
    $response->setContent($output);
    return $response;
  }

  /**
   * Builds the response.
   */
  public function build($node = NULL) {
    /* Asset checker
     *
    \Drupal\Core\Cache\Cache::invalidateTags(['library_info']);
    $assets = new \Drupal\Core\Asset\AttachedAssets();
    $assets->setLibraries(['crelb5/email']);
    $asset_resolver = \Drupal::service('asset.resolver');
    $css = '---';
    foreach ($asset_resolver->getCssAssets($assets, TRUE) as $file) {
    $css .= file_get_contents($file['data']);
    }
    $response = new Response();
    $response->setContent($css);
    return $response;
     */

    $cache = new MailCacheNone();
    $subscriber = Subscriber::loadByMail("hya27psuh@mozmail.com", 'create', $this->languageManager->getCurrentLanguage());
    $mail = new MailEntity($node, $subscriber, $cache);
    $params['simplenews_mail'] = $mail;
    // $mail->setParam('simplenews_subscriber', $subscriber);
    // Rebuild cache by assetResolver (Cache::PERMANENT) in Drupal\Core\Asset\AssetResolver::getCssAssets
    \Drupal::service('asset.css.collection_optimizer')->deleteAll();
    $this->state()->set('system.css_js_query_string', base_convert($this->time->getCurrentTime(), 10, 36));
    // The only valid clearer:
    Cache::invalidateTags(['library_info']);

    // Act as in https://git.drupalcode.org/project/symfony_mailer/-/blob/1.x/src/MailManagerReplacement.php#L75 mail method.
    $module = 'simplenews';
    $key = $mail->getKey();
    $to = $mail->getRecipient();
    $langcode = $mail->getLanguage();
    try {
      $reply = $mail->getFromFormatted();
    }
    catch (\Throwable $e) {
      $this
        ->messenger()
        ->addError($this
          ->t('Simplenews is not yet configured and it cannot render the result'));
      return [];
    }

    $message = [
      'id' => $module . '_' . $key,
      'module' => $module,
      'key' => $key,
      'to' => $to,
      'langcode' => $langcode,
      'params' => $params,
      'reply-to' => $reply,
      'send' => FALSE,
      'subject' => 'MAIL_PREVIEW',
    ];
    $email = \Drupal::service('plugin.manager.email_builder')->createInstanceFromMessage($message)->fromArray($this->emailFactory, $message);
    $email->setVariable('mail_preview', TRUE);
    // It's not send as there's simplenews_render_flow_mailer_init that sets transport to null.
    $message['result'] = $email->send();
    $response = new Response();
    $response->setContent($email->getHtmlBody());
    // $response->headers->set('Content-Type', 'text/xml');
    return $response;
  }

  /**
   * Checks access for mail_preview route.
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *   Run access checks for this account.
   * @param \Drupal\node\NodeInterface $node
   *   The node that is being parent of subpages.
   *
   * @return \Drupal\Core\Access\AccessResultInterface
   *   The access result.
   */
  public function access(AccountInterface $account, NodeInterface $node): AccessResultInterface {
    $content_type = $node->type->entity;
    $render_field = $content_type->getThirdPartySetting('simplenews_render_flow', 'render_field');
    $simplenews_enabled = \Drupal::service('entity_field.manager')->getFieldDefinitions('node', $content_type->id());
    if (isset($render_field) &&
      $node->hasField($render_field) &&
      $simplenews_enabled
    ) {
      return $node->access('view', $account, TRUE);
    }
    return AccessResult::forbidden();
  }

}
