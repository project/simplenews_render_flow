<?php

declare(strict_types = 1);

namespace Drupal\simplenews_render_flow\Plugin\Block;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Cache\Cache;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\node\NodeInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a newsletter issue render preview block.
 *
 * @Block(
 *   id = "simplenews_render_flow_newsletter_issue_render_preview",
 *   admin_label = @Translation("Newsletter issue render preview"),
 *   category = @Translation("Content"),
 * )
 */
final class NewsletterIssueRenderPreviewBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * Constructs the plugin instance.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    private readonly RouteMatchInterface $routeMatch,
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition): self {
    return new self(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('current_route_match'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function build(): array {
    /** @var \Drupal\node\NodeInterface $node */
    $node = $this->routeMatch->getParameter('node');

    if (!($node instanceof NodeInterface)) {
      return [];
    }

    $build = [];
    $content_type = $node->type->entity;
    $render_field = $content_type->getThirdPartySetting('simplenews_render_flow', 'render_field');
    $view_mode = $content_type->getThirdPartySetting('simplenews_render_flow', 'view_mode');

    if (isset($render_field) && $node->hasField($render_field) && isset($view_mode)) {
      $build['content'] = $node->get($render_field)->view('email_html');
    }

    return $build;
  }

  /**
   * {@inheritdoc}
   */
  protected function blockAccess(AccountInterface $account): AccessResult {
    /** @var \Drupal\node\NodeInterface $node */
    $node = $this->routeMatch->getParameter('node');

    if ($node) {

      $content_type = $node->type->entity;
      $render_field = $content_type->getThirdPartySetting('simplenews_render_flow', 'render_field');
      $simplenews_enabled = \Drupal::service('entity_field.manager')->getFieldDefinitions('node', $content_type->id());
      if (isset($render_field) &&
        $node->hasField($render_field) &&
        $simplenews_enabled
      ) {
        return $node->access('edit', $account, TRUE);
      }
    }
    return AccessResult::forbidden();

  }

  /**
   * {@inheritdoc}
   */
  public function getCacheTags() {
    if ($node = \Drupal::routeMatch()->getParameter('node')) {
      return Cache::mergeTags(parent::getCacheTags(), ['node:' . $node->id()]);
    }
    else {
      return parent::getCacheTags();
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheContexts() {
    // Every new route this block will rebuild.
    return Cache::mergeContexts(parent::getCacheContexts(), ['route']);
  }

}
